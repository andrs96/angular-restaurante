import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../../../services/auth.service";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {HeadersService} from "../../../services/headers.service";

declare var $: any;

@Component({
    selector: 'app-pratos-criar',
    templateUrl: './pratos-criar.component.html',
    styleUrls: ['./pratos-criar.component.css']
})
export class PratosCriarComponent implements OnInit {
    dish:any = {}

    constructor(private router: Router, private http: HttpClient, private options: HeadersService) { }

    ngOnInit() {
        $('.save-prato').modal('show');

        $('.save-prato').on('hidden.bs.modal', () => this.router.navigate(['pratos']));

        $('.custom-file-input').on('change', function() {
            let fileName = $(this).val().split('\\').pop();
            $(this).next('.custom-file-label').addClass("selected").html(fileName);
        });
    }

    addFile(e) {
        this.dish.photo = e.target.files[0];
    }

    save() {
        const headers = this.options.headers();

        if (!this.dish.photo) {
            alert('Selecione uma foto antes.');
            return;
        }

        let formData = new FormData;
        formData.append('photo', this.dish.photo);
        formData.append('name', this.dish.name);
        formData.append('description', this.dish.description);
        formData.append('price', this.dish.price);
        formData.append('restaurant_id', this.dish.restaurant_id);

        this.http
            .post(environment.server_url + 'restaurant/dishes', formData, {headers})
            .subscribe(() => {
                alert('success');
            }, error => {
                alert('error');
            });
    }
}
