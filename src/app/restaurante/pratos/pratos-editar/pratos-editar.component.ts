import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {HeadersService} from "../../../services/headers.service";
import {environment} from "../../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {t} from "@angular/core/src/render3";

declare var $: any;

@Component({
    selector: 'app-pratos-editar',
    templateUrl: './pratos-editar.component.html',
    styleUrls: ['./pratos-editar.component.css']
})
export class PratosEditarComponent implements OnInit {
    dish: any = {};

    constructor(private router: Router, private route: ActivatedRoute, private options: HeadersService, private http: HttpClient) {
    }

    ngOnInit() {
        $('.save-prato').modal('show');

        $('.save-prato').on('hidden.bs.modal', () => this.router.navigate(['pratos']));


        const headers = this.options.headers();

        this.route.params.subscribe(params => {
            this.http
                .get(environment.server_url + 'restaurant/dishes/' + params['id'], {headers})
                .subscribe((res) => {
                    this.dish = res;
                    console.log(res);

                });
        });

    }

    addFile(e) {
        this.dish.photo = e.target.files[0];
    }

    save() {
        const headers = this.options.headers();

        let formdata = this.dish;

        if (this.dish.photo) {
            formdata = new FormData;
            formdata.append('photo', this.dish.photo);
            formdata.append('name', this.dish.name);
            formdata.append('description', this.dish.description);
            formdata.append('price', this.dish.price);
            formdata.append('restaurant_id', this.dish.restaurant_id);
        }

        this.http
            .post(environment.server_url + 'restaurant/dishes/' + this.dish.id, formdata, {headers})
            .subscribe(() => {
                alert('success');
            }, error => {
                alert('error');
            });
    }
}
