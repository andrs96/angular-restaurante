import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {JwtTokenService} from "../../services/jwt-token.service";
import {HttpClient} from "@angular/common/http";
import {HeadersService} from "../../services/headers.service";

@Component({
    selector: 'app-pratos',
    templateUrl: './pratos.component.html',
    styleUrls: ['./pratos.component.css']
})
export class PratosComponent implements OnInit {

    dishes: any = [];

    constructor(private http: HttpClient, private options: HeadersService, private jwtToken: JwtTokenService) { }

    ngOnInit() {
        this.getDishes()
    }

    getDishes() {
        const headers = this.options.headers();
        this.http
            .get(environment.server_url + 'restaurant/dishes', {headers})
            .subscribe(
                response => {
                    this.dishes = (<any>response).data;
                },
                error => {
                    this.http
                        .post(environment.server_url + 'auth/refresh_token', {}, {headers})
                        .subscribe(response => this.jwtToken.setToken(response));
                }
            );
    }

    deleteDish(dish)
    {
        const headers = this.options.headers();
        this.http
            .delete(environment.server_url + 'restaurant/dishes/' + dish.id, {headers})
            .subscribe(() => this.getDishes(),
            error => {
                alert('Erro ao enviar, tente novamente ou contate o administrador.');
            });
    }
}
