import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HeadersService} from "../../services/headers.service";
import {environment} from "../../../environments/environment";

@Component({
    selector: 'app-perfil',
    templateUrl: './perfil.component.html',
    styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
    user: any = {};

    constructor(private http: HttpClient, private options: HeadersService) { }

    ngOnInit() {
        const headers = this.options.headers();

        this.http
            .get(environment.server_url + 'me', {headers})
            .subscribe((res) => {
                this.user = res;
            }, error => {
                alert('Ocorreu algum erro.');
            });
    }

    updateUser() {
/*        if (this.user.password) {
            if (this.user.password.length < 6) {
                return alert('A senha deve ter no min 6 caracteres.');
            } else if (this.user.password !== this.user.password_confirm) {
                return alert('Por favor, verifique as senhas.');
            }
        }*/

        const headers = this.options.headers();

        this.http
            .put(environment.server_url + 'change-password', this.user, {headers})
            .subscribe(() => {
                alert('Atualizado!');
            }, error => {
                alert('Ocorreu algum erro.');
            });
    }
}
