import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {HeadersService} from "../../services/headers.service";
import {JwtTokenService} from "../../services/jwt-token.service";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  constructor(){ }

  ngOnInit() {
  }
}
