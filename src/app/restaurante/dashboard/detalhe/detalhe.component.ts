import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

declare var $:any;

@Component({
  selector: 'app-detalhe',
  templateUrl: './detalhe.component.html',
  styleUrls: ['./detalhe.component.css']
})
export class DetalheComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    $('#detalhe').modal('show');

    $('#detalhe').on('hidden.bs.modal', () => this.router.navigate(['dashboard']));
  }

}
