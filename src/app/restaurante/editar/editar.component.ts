import { Component, OnInit } from '@angular/core';
import {environment} from "../../../environments/environment";
import {JwtTokenService} from "../../services/jwt-token.service";
import {HttpClient} from "@angular/common/http";
import {HeadersService} from "../../services/headers.service";

declare var $:any;

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {
  restaurant: any = {};
  photos: any = [];
  address: any = {};
  dragging: boolean = false;
  upload_status: string = 'not';
  restaurantPhoto: any = null;

  constructor(private http: HttpClient, private options: HeadersService, private jwtToken: JwtTokenService) { }

  ngOnInit() {
    this.getRestaurants();
    this.getPhotos();

    $('.custom-file-input').on('change', function() {
      let fileName = $(this).val().split('\\').pop();
      $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });
  }

  getRestaurants() {
    const headers = this.options.headers();
    this.http
      .get(environment.server_url + 'restaurant/user', {headers})
      .subscribe(
        response => {
            this.restaurant = response;
            this.address = (<any>response).address;
        },
          error => {
            this.http
            .post(environment.server_url + 'auth/refresh_token', {}, {headers})
            .subscribe(response => this.jwtToken.setToken(response));
          }
      );
  }

  getPhotos() {
    const headers = this.options.headers();
    this.http
      .get(environment.server_url + 'restaurant/photos', {headers})
      .subscribe(
        response => {
            this.photos = response;
        },
          error => {
            this.http
            .post(environment.server_url + 'auth/refresh_token', {}, {headers})
            .subscribe(response => this.jwtToken.setToken(response));
          }
      );
  }

  save() {
    const headers = this.options.headers();
    this.http
      .put(environment.server_url + `restaurant/address`, this.address, {headers})
      .subscribe(
        response => {
          this.address = response;
        },
        error => {
          this.http
          .post(environment.server_url + 'auth/refresh_token', {}, {headers})
          .subscribe(response => this.jwtToken.setToken(response));
        }
      );
  }

    searchCep() {
      let cep = this.address.cep || null;

      if (cep && cep.length === 8) {
        this.http.get(`https://viacep.com.br/ws/${cep}/json/`).subscribe(
          res => {
          this.address = {
              cep: cep,
              address: (<any>res).logradouro,
              city: (<any>res).localidade,
              neighborhood: (<any>res).bairro,
              state: (<any>res).uf
            }
          }
        )};
    }

  upload(e) {
      e.preventDefault();
      const headers = this.options.headers();

      let image_url: any = null;
      if (e.dataTransfer) {
          image_url = e.dataTransfer.files[0];  // foi pelo dragover
      } else {
          image_url = e.target.files[0];        // foi pelo click
      }

      this.upload_status = 'sending';

      let formData = new FormData();
      formData.append('photo', image_url);

      this.http
          .post(environment.server_url + 'restaurant/upload', formData, {headers})
          .subscribe(() => {
              this.upload_status = 'success';
          }, error => {
              this.upload_status = 'error';
          });
  }

  dragover(e) {
    e.stopPropagation();
    e.preventDefault();
    this.dragging = true;
  }

    preparePhoto(e)
    {
        let image_url = e.target.files[0];
        let formData = new FormData();
        formData.append('restaurant_id', this.restaurant.id);
        formData.append('url', image_url);
        this.restaurantPhoto = formData;
    }

    sendPhoto()
    {
        if (this.restaurantPhoto === null) {
            alert('Selecione uma imagem antes.');
            return;
        }

        const headers = this.options.headers();

        this.http
            .post(environment.server_url + 'restaurant/photos', this.restaurantPhoto, {headers})
            .subscribe(() => this.getPhotos(),
            error => {
                alert('Erro ao enviar, tente novamente ou contate o administrador.');
            });
    }

    deletePhoto(photo)
    {
        const headers = this.options.headers();
        this.http
            .delete(environment.server_url + 'restaurant/photos/' + photo.id, {headers})
            .subscribe(() => this.getPhotos(),
            error => {
                alert('Erro ao enviar, tente novamente ou contate o administrador.');
            });
    }
}
