import {Component, OnInit} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {JwtTokenService} from "../../services/jwt-token.service";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
    user: any = {};

    constructor(private http: HttpClient, private jwtToken: JwtTokenService, private router: Router, private auth: AuthService) { }

    ngOnInit() { }

    login() {
        console.log(this.user);
        this.http.post(environment.server_url + 'auth/login', this.user)
            .subscribe(response => {
                this.auth.check = true;
                this.jwtToken.setToken(response);
                this.router.navigate(['dashboard']);
            });
    }
}