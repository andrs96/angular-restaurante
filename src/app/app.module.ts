import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { NavbarComponent } from './bootstrap/navbar/navbar.component';
import { FooterComponent } from './bootstrap/footer/footer.component';
import { DashboardComponent } from './restaurante/dashboard/dashboard.component';
import {RouterModule, Routes} from "@angular/router";
import { PaginatorComponent } from './bootstrap/paginator/paginator.component';
import { DetalheComponent } from './restaurante/dashboard/detalhe/detalhe.component';
import { PratosComponent } from './restaurante/pratos/pratos.component';
import { EditarComponent } from './restaurante/editar/editar.component';
import { PerfilComponent } from './restaurante/perfil/perfil.component';
import { SenhaComponent } from './restaurante/senha/senha.component';
import { PratosEditarComponent } from './restaurante/pratos/pratos-editar/pratos-editar.component';
import { PratosCriarComponent } from './restaurante/pratos/pratos-criar/pratos-criar.component';
import { HeadersService } from "./services/headers.service";
import { JwtTokenService } from "./services/jwt-token.service";
import { LocalStorageService } from "./services/local-storage.service";
import { HttpClientModule } from "@angular/common/http";
import {AuthService} from "./services/auth.service";
import {AuthGuardRouterService} from "./services/auth-guard-router.service";
import { LoginComponent } from './login/login/login.component';

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: '', canActivate:[AuthGuardRouterService], children: [
    {
      path: 'dashboard', component: DashboardComponent,
      children: [
        {path: 'detalhe/:id', component: DetalheComponent}
      ]
    },
    {
      path: 'pratos', component: PratosComponent,
      children: [
        {path: 'criar', component: PratosCriarComponent},
        {path: 'editar/:id', component: PratosEditarComponent}
      ]
    },
    {path: 'editar', component: EditarComponent},
    {path: 'perfil', component: PerfilComponent},
    {path: 'senha', component: SenhaComponent}
  ]}
];

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    DashboardComponent,
    PaginatorComponent,
    DetalheComponent,
    PratosComponent,
    EditarComponent,
    PerfilComponent,
    SenhaComponent,
    PratosEditarComponent,
    PratosCriarComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule
  ],
  providers: [
      HeadersService,
      JwtTokenService,
      LocalStorageService,
      AuthService,
      AuthGuardRouterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
